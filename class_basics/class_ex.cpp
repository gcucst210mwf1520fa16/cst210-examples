#include <iostream>

using namespace std;

class Dog {
  public:
  string name;
  string breed;
  int age;
  float happiness;
  Dog(int age_, string breed_, string given_name) {
    name = given_name;
    age = age_;
    breed = breed_;
    health = "Healthy";
  }

  void petDog() {
    cout << name << " wags its tail." << endl;
  }

  string checkHealth() {
    return health;
  }

  private:
  string health;

};

int main() {

  cout << "Begin" << endl;

  Dog d(5, "Hound", "BoBo");
  //Dog d;
  //d.name = "Phydeaux";
  //d.breed = "German Shepard";
  //d.age = 5;
  d.happiness = 10;

  cout << d.name << " is a " << d.breed << endl;

  if (d.age > 7) {
    cout << d.name << " is an older dog." << endl;
  }
  else if (d.age < 3) {
    cout << d.name << " is a puppy." << endl;
  } 
  else {
    cout << d.name << " is a middle-aged dog." << endl;
  }

  cout << "Vets diagnosis: " << d.checkHealth() << endl;

  d.petDog();

  //cout << d.health << endl;

  cout << "End" << endl;

  return 0;
}
