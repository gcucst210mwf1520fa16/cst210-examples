#include <iostream>
#include "point3.h"

using std::cout;
using std::endl;

int main() {
  Point2D point1(5,10);
  Point2D point2(20,40);

  Point2D point3 = point1.sum(point2);         //( 5,10) + (20,40) = ( 25, 50)
  Point2D point4 = point2 + point3;            //(20,40) + (25,50) = ( 45, 90)

  Point2D point5 = point3.scale(5);            //(25,50) * 5       = (125,250)

  Point2D point6 = point3 * 10;                //(25,50) * 10      = (250,500)
  Point2D point7 = 10 * point3;                //     10 * (25.50) = (250,500)

  Point2D point8 = point1.difference(point2);  //( 5,10) - (20,40) = (-15,-30)
  Point2D point9 = point2 - point3;            //(20,40) - (25,50) = ( -5,-10)

  Point2D pointA = point1 * 0.5;               //( 5,10) * 0.5     = (2.5,  5)
  Point2D pointB = point1 * 1.5;               //( 5,10) * 1.5     = (7.5, 15)

  cout << "point1.x: " << point1.x << endl;    // 5
  cout << "point1.y: " << point1.y << endl;    // 10
  cout << "point2.x: " << point2.x << endl;    // 20
  cout << "point2.y: " << point2.y << endl;    // 40
  cout << "point3.x: " << point3.x << endl;    // 25
  cout << "point3.y: " << point3.y << endl;    // 50
  cout << "point4.x: " << point4.x << endl;    // 45
  cout << "point4.y: " << point4.y << endl;    // 90
  cout << "point5.x: " << point5.x << endl;    // 125
  cout << "point5.y: " << point5.y << endl;    // 250
  cout << "point6.x: " << point6.x << endl;    // 250
  cout << "point6.y: " << point6.y << endl;    // 500
  cout << "point7.x: " << point7.x << endl;    // 250
  cout << "point7.y: " << point7.y << endl;    // 500
  cout << "point8.x: " << point8.x << endl;    // -15
  cout << "point8.y: " << point8.y << endl;    // -30
  cout << "point9.x: " << point9.x << endl;    // -5
  cout << "point9.y: " << point9.y << endl;    // -10

  cout << "pointA.x: " << pointA.x << endl;    // 2.5
  cout << "pointA.y: " << pointA.y << endl;    // 5
  cout << "pointB.x: " << pointB.x << endl;    // 7.5
  cout << "pointB.y: " << pointB.y << endl;    // 15
  return 0;
}
