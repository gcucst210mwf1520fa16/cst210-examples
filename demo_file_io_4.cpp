/***************************************************************************
* Filename: demo_file_io_4.cpp
* Author: Luke Kanuchok
* Date: 10/28/2016
* 
* Demonstrate how File I/O works in C++
* - Create an object with multiple data fields
* - Write that output to a file
***************************************************************************/

#include <iostream>  //for std::cout, std::endl
#include <fstream>   //for std::ofstream
#include <string>    //for std::string

using std::cout;
using std::endl;
using std::string;
using std::ofstream;
using std::vector;
using std::getline;

class Whatchamacallit {
  public:
  Whatchamacallit() {
    type = 0;
  }

  Whatchamacallit(string n, int ty, double temp) {
    name = n;
    type = ty;
    temperature = temp;
  }

  void write(ofstream& os) {
    //- Write this object to the output file stream -
    //  NOTE: should this code add the endl?
    os << type << " " << name << " " << temperature << endl;
  }

  void print() {
    cout << "This whatchamacallit has:" << endl;
    cout << " type:" << type << endl;
    cout << " name:" << name << endl;
    cout << " temp:" << temperature << endl;
  }

  private:
  string name;
  int type;
  double temperature;
};

int main() {

  ofstream ofs;   //ofstream objects are an Output File STREAM

  //- Opening output file -
  cout << "Opening the output file..." << endl;
  ofs.open("demo_output_filename4.txt");

  //- Create sample object -
  Whatchamacallit w("I'm me!", 1, 98.6);

  //- Display the object -
  w.print();

  //- Write object to file -
  w.write(ofs);

  //- Closing files -
  cout << "Closing the output file..." << endl;
  ofs.close();

  //- Finished -
  cout << "Done." << endl;

  return 0;
}
