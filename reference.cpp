#include <iostream>
using namespace std;

void addtwo(int val) {
  cout << "  in addtwo: val = " << val << endl;
  cout << "  Adding 2..." << endl;
  val = val + 2;
  cout << "  in addtwo: val = " << val << endl;
}

void addtwo2(int & val2) {
  cout << "  in addtwo2: val2 = " << val2 << endl;
  cout << "  Adding 2..." << endl;
  val2 = val2 + 2;
  cout << "  in addtwo2: val2 = " << val2 << endl;
}

int addthree(int val) {
  cout << "  in addthree: val = " << val << endl;
  cout << "  Adding 3..." << endl;
  val = val + 3;
  cout << "  in addthree: val = " << val << endl;
  cout << "  returning val (" << val << ")" << endl;
  return val;
}

int addthree2(int & val2) {
  cout << "  in addthree2: val2 = " << val2 << endl;
  cout << "  Adding 3..." << endl;
  val2 = val2 + 3;
  cout << "  in addthree2: val2 = " << val2 << endl;
  cout << "  returning val2 (" << val2 << ")" << endl;
  return val2;
}

int main() {

  int x;

  x = 5;

  cout << "x = " << x << endl;

  addtwo(x);

  cout << "x = " << x << endl;

  addtwo2(x);

  cout << "x = " << x << endl;

  int y = 20;
  x = addthree(y);
  cout << "Returned val stored in x" << endl;

  cout << "x = " << x << endl;
  cout << "y = " << y << endl;
  
  x = addthree2(y);
  cout << "Returned val stored in x" << endl;

  cout << "x = " << x << endl;
  cout << "y = " << y << endl;
  

  return 0;
}
