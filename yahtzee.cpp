/*******************************************************************************
*
* Program: yahtzee
*
* Definition:
* - A player will roll some dice, and record the relevant values. If there are
*   no available blanks, a zero will be entered in the blank of the players
*   choice. The game is over when all blanks are filled. Scoring will follow the
*   following (simplified) system:
*   - Ones (sum of the ones of the dice)
*   - Twos (sum of the twos of the dice)
*   - Threes (sum of the threes of the dice)
*   - Fours (sum of the fours of the dice)
*   - Fives (sum of the fives of the dice)
*   - Sixes (sum of the sizes of the dice)
*
* Requirements:
* - The dice will have 6 sides, and are contained by a cup.
* - The cup will roll all of the dice, and report the set of values rolled.
*   Players will be given a chance to re-roll up to twice, keeping values
*   as desired. Players must keep the final results (third roll).
* - The tracking sheet will record the desired values.
* - The player class will provide a standard interface for user and computer
*   players.
* - The game class will run the game loop.
*
* Algorithm:
* - 
*******************************************************************************/
#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <sstream>

using namespace std;

const int die_sides = 6;

string int_to_string(int & num) {
  ostringstream oss;
  oss << num;
  return oss.str();
}

class Die {
  //is_held indicates whether a die's value is being held during a re-roll
  bool is_held;
  //Current value of the die
  int face_val;
  public:
  Die() {
    is_held = false;
    face_val = 0;
  }

  Die(int val) {
    is_held = false;
    face_val = val;
  }

  int roll() {
    //Return a randomized die roll between 1 and die_sides inclusive

    if (!is_held) {
      face_val = rand() % die_sides + 1;
    }

    return face_val;
  }

  inline void hold_die() {
    is_held = true;
  }

  inline void release_die() {
    is_held = false;
  }

  inline int get_val() {
    return face_val;
  }

  friend ostream& operator<<(ostream& os, const Die &d) {
    os << d.face_val;
    return os;
  }
};

class Cup {
  private:
  Die * dice;
  int num_dice;

  int count_values(int val) {
    //Return the number of times that 'val' is shown on the dice
    int retVal = 0;
    retVal = 0;  //FIXME: Replace this line with code that works!
    return retVal;
  }

  const string create_roll_string() {
    //Return a string of space seperated values of the counts on the dice
    string retVal = "";
    retVal = "1 2 3 4 5";  //FIXME: Replace this line with code that works!
    return retVal;
  }

  public:
  Cup(int number_of_dice = 5) {
    if (number_of_dice < 1)
      throw "Too few dice: "+int_to_string(number_of_dice);
    num_dice = number_of_dice;

    dice = new Die[num_dice];
    for (int d = 0; d < num_dice; ++d) {
      //Create and add new die to dice array
      Die* new_die = new Die;
      dice[d] = *new_die;
    }
  }

  inline void hold_die(int idx) {
    dice[idx].hold_die();
  }

  inline void release_die(int idx) {
    dice[idx].release_die();
  }

  void roll_dice() {
    for (int i = 0; i < num_dice; ++i)
      dice[i].roll();
  }

  friend ostream& operator<<(ostream& os, const Cup &c) {
    for (int i = 0; i < c.num_dice; ++i) {
      os << c.dice[i];
      if (i < c.num_dice-1)
        os << " ";
    }
    return os;
  }

  void score_roll(int blank, int & score, string & roll_string) {
    switch (blank) {
      case 0:  //Yahtzee
        if (count_values(1) == 5 ||
	    count_values(2) == 5 ||
	    count_values(3) == 5 ||
	    count_values(4) == 5 ||
	    count_values(5) == 5 ||
	    count_values(6) == 5)
	  //We have a Yahtzee!
          score = 50;
	else
	  score = 0;
	break;
      case 1:  //Ones
        score = count_values(1);
	break;
      case 2:  //Twos
        score = count_values(2);
	break;
      case 3:  //Threes
        score = count_values(3);
	break;
      case 4:  //Fours
        score = count_values(4);
	break;
      case 5:  //Fives
        score = count_values(5);
	break;
      case 6:  //Sixes
        score = count_values(6);
	break;
      default:
        throw "No blank of type:"+int_to_string(blank);
    }
    roll_string = create_roll_string();
  }
};

struct ScoreEntry {
  string name;
  string roll;
  int score;
  bool is_recorded;
};

class TrackingSheet {
  private:
  ScoreEntry entries[7];
  public:
  TrackingSheet() {
    //FIXME: The common parts of these lines should be in a loop!
    entries[0].name = "Yahtzee";
    entries[0].is_recorded = false;
    entries[0].score = 0;
    entries[0].roll = "Not yet recorded";

    entries[1].name = "Ones";
    entries[1].is_recorded = false;
    entries[1].score = 0;
    entries[1].roll = "Not yet recorded";

    entries[2].name = "Twos";
    entries[2].is_recorded = false;
    entries[2].score = 0;
    entries[2].roll = "Not yet recorded";

    entries[3].name = "Threes";
    entries[3].is_recorded = false;
    entries[3].score = 0;
    entries[3].roll = "Not yet recorded";

    entries[4].name = "Fours";
    entries[4].is_recorded = false;
    entries[4].score = 0;
    entries[4].roll = "Not yet recorded";

    entries[5].name = "Fives";
    entries[5].is_recorded = false;
    entries[5].score = 0;
    entries[5].roll = "Not yet recorded";

    entries[6].name = "Sixes";
    entries[6].is_recorded = false;
    entries[6].score = 0;
    entries[6].roll = "Not yet recorded";
  }

  void set(int idx, string roll_str, int score) {
    entries[idx].roll = roll_str;
    entries[idx].score = score;
    entries[idx].is_recorded = true;
  }

  void print() {
    for (int n = 0; n < 7; ++n) {
      cout << entries[n].name;
      cout << ": ";
      cout << entries[n].score;
      cout << "   (roll: ";
      cout << entries[n].roll;
      cout << ")" << endl;
    }
  }
};

class Player {
  private:
  Cup cup;
  int final_score;

  public:
  Player() {}
  void take_turn() {
    //FIXME: How does a player take a turn?
  }

  int calculate_total_score() {
    int total = 0;

    //FIXME: Fill in code to calculate the current score!

    final_score = total;
    return final_score;
  }
};

class Game {
  private:
  int total_turns;
  Player players[2];  //FIXME: Should be a configurable number of players!

  public:
  Game() {
    //Initialize the game
    total_turns = 7; //FIXME: How should this be determined?
  }

  void play() {
    for (int turn = 0; turn < total_turns; ++turn) {
      //Each player should take a turn
      players[0].take_turn();
      players[1].take_turn();
    }

    //Determine the final scores
    for (int i = 0; i < 2; ++i) {
      //FIXME: How should we track the player with the final score?
      cout << "Player " << i << " got ";
      cout << players[i].calculate_total_score();
      cout << " points.\n";
    }

    //Announce the winner
    //FIXME: Who is the winner?
  }
};

int main() {
  //Seed the random library with the currect time
  srand(time(NULL));

  /*
  Die die;
  cout << die.roll();
  for (int i = 0; i < 4; ++i)
    cout << " " << die.roll();
  cout << endl;
  */

  Cup cup;
  try {
    int score;
    string roll_string;
    cup.roll_dice();
    cout << cup << endl;
    cup.score_roll(2, score, roll_string);
    cout << "The roll \"" << roll_string << "\" scored " << score;
    cout << " points in the Twos.\n";
  }
  catch (string &s) {
    cout << "ERROR - " << s << endl;
  }

  TrackingSheet ts;
  ts.print();

  Player p1;
  
  Game game;

  return 0;
}
