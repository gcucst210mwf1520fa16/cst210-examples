/***************************************************************************
* Filename: demo_file_io_5.cpp
* Author: Luke Kanuchok
* Date: 10/28/2016
* 
* Demonstrate how File I/O works in C++
* - Read data from file
* - Create an object with data taken from file
***************************************************************************/

#include <iostream>  //for std::cout, std::endl
#include <fstream>   //for std::ifstream
#include <string>    //for std::string

using std::cout;
using std::endl;
using std::string;
using std::ifstream;
//using std::getline;

class Whatchamacallit {
  public:
  Whatchamacallit() {
    type = 0;
  }

  Whatchamacallit(string n, int ty, double temp) {
    name = n;
    type = ty;
    temperature = temp;
  }

  void read(ifstream& is) {
    is >> type >> name >> temperature;
  }

  //void write(ofstream& os) {
    ////- Write this object to the output file stream -
    ////  NOTE: should this code add the endl?
    //os << type << " " << name << " " << temperature << endl;
  //}

  void print() {
    cout << "This whatchamacallit has:" << endl;
    cout << " type:" << type << endl;
    cout << " name:" << name << endl;
    cout << " temp:" << temperature << endl;
  }

  private:
  string name;
  int type;
  double temperature;
};

int main() {

  ifstream ifs;   //ifstream objects are an Input File STREAM

  //- Opening output file -
  cout << "Opening the input file..." << endl;
  ifs.open("demo_output_filename4.txt");   //From demo 4

  //- Create sample object -
  Whatchamacallit w;

  //- Read object in from file -
  w.read(ifs);

  //- Closing files -
  cout << "Closing the input file..." << endl;
  ifs.close();

  //- Display the object -
  w.print();

  //- Finished -
  cout << "Done." << endl;

  return 0;
}
