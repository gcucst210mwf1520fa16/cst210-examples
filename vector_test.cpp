/*******************************************************************************
* 
* Name: vector_test.cpp
* Author: Luke Kanuchok
* Date: 10/9/2016
*
* Purpose: Exercise the vector clas to demonstrate vector usage.
*
*******************************************************************************/
#include <iostream>   //std::cout, std::endl
#include <vector>     //std::vector
#include <stdexcept>  //std::out_of_range

//These act like our "using namespace std;" for only these particular names.
using std::cout;
using std::endl;
using std::vector;

int main() {

  vector<int> vec;  //Default constructor

  vec.push_back(5);
  vec.push_back(6);
  vec.push_back(7);

  vec.pop_back();

  cout << "vector size:" << vec.size() << endl;  
  cout << "vector capacity:" << vec.capacity() << endl;

  cout << "vec[0]:" << vec[0] << endl;
  //cout << "vector:" << vec << endl; //This is not supported in std::vector

  vector<int> vec2(vec);  //Copy constructor
  
  if (vec.empty())
    cout << "vec is empty" << endl;
  else
    cout << "vec is NOT empty" << endl;

  vec.clear();

  cout << "vec2 front:" << vec2.front() << endl;
  cout << "vec2 back:" << vec2.back() << endl;

  cout << "vec2.at(1):" << vec2.at(1) << endl;
  try {
    cout << "vec2.at(4):" << vec2.at(4) << endl;
  }
  catch (const std::out_of_range& oor) {
    cout << "No element at index 4 in vec2" << endl;
  }
  
  cout << "vector capacity before resize(10):" << vec.capacity() << endl;
  vec.resize(10);
  cout << "vector capacity after resize(10):" << vec.capacity() << endl;

  for (int i = 0; i < 10; ++i) {
    cout << vec[i];
    if (i < 9) cout << " ";
  }
  cout << endl;

  cout << "vector capacity before reserve(15):" << vec.capacity() << endl;
  vec.reserve(15);
  cout << "vector capacity after reserve(15):" << vec.capacity() << endl;

  for (int i = 0; i < 15; ++i) {
    cout << vec[i];
    if (i < 14) cout << " ";
  }
  cout << endl;

  return 0;
}
