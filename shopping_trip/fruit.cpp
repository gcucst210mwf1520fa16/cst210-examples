/***************************************************************************
* Filename: fruit.cpp
* Author: Luke Kanuchok
* Date: 2016/11/02
*
* Class: Fruit
*
* Class components:
*   Public data members:
*    - NONE
*   Public methods:
*    - Name: Fruit (parameterized constructor)
*      - Return type: NONE
*      - Arguments:
*        - Name: c
*          - Type: float 
*          - Intent: Cost of the Fruit
*        - Name: q
*          - Type: double 
*          - Intent: Quality of the Fruit
*      - Intent: Construct a fruit. Use a randomly generated number to set
*                desired smell.
*    - Name: sniff
*      - Return type: virtual bool
*      - Arguments:
*        - Name: desired_smell
*        - Type: float
*        - Intent: Level of good smell required.
*      - Intent: Returns true if fruit smells good, false otherwise.
*   Protected data members:
*    - NONE
*   Protected methods:
*    - NONE
*   Private data members:
*    - Name: smell_value
*      - Type: float
*      - Intent: How good this fruit smells (0 = Awful, 1 = Awesome.)
*   Private methods:
*    - NONE
***************************************************************************/

#include "fruit.h"

#include <iostream> //For std::cout, std::endl
using std::cout;
using std::endl;


//Standard headers
#include <cstdlib>  //For std::rand, std::RAND_MAX
using std::rand;
//using std::RAND_MAX;

Fruit::Fruit(float c, double q) : Salable(c, q) {
  smell_value = static_cast<float> (rand()) / RAND_MAX;
}

bool Fruit::sniff(float desired_smell) {
  cout << "I don't like the smell of generic fruit." << endl;
  return false;
}
