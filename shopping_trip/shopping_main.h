
//Load the necessary libraries
#include <iostream> //For std::cout, std::endl;
#include <cstdlib>  //For std::rand, std::srand
#include <ctime>    //For std::time
#include <vector>   //For std::vector
#include <iomanip>  //For std::setprecision
#include "banana.h"
#include "pineapple.h"
#include "fruit.h"

//Prepare some names
using std::cout;
using std::endl;
using std::rand;
using std::srand;
using std::time;
using std::vector;
using std::setprecision;
using std::fixed;
