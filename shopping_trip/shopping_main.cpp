/***************************************************************************
* Filename: shopping_main.cpp
* Author: Luke Kanuchok
* Date: 2016/11/02
*
* Program description:
*  This program demonstrates using inherited classes to simulate a shopping
*  trip. An array of fruit is created, and populated with a variety of
*  bananas and pineapples. Then, a series of checks for fresh fruit is
*  performed, and each fruit that smells goo enough is bought until the
*  available money is gone. The purchased fruit are printed out, along with
*  the final purchase price.
***************************************************************************/

#include "shopping_main.h"

const int fruitstand_size = 10;
const float cost_banana    = 0.95;
const float cost_pineapple = 2.95;
const int num_fruit_to_buy = 5;

Fruit* create_random_fruit() {
  Fruit* fp;  //Pointer to a created fruit
  int rand_val = (rand() % 2);
  double qual_num = static_cast<float>(rand()) / RAND_MAX;

  switch (rand_val) {
    case 0:
      //Create a banana
      fp = new Banana(cost_banana, qual_num);
      break;
    case 1:
      //Create a pineapple
      fp = new Pineapple(cost_pineapple, qual_num);
    break;
  }
  return fp;
}

void setup_shopping_trip(float& money, Fruit * fs[]) {
  //Seed the random number generator
  srand(time(NULL));

  //Set the output format for our floating point prices
  cout << fixed << setprecision(2);

  money = 10.00;

  //Populate fruitstand with fruit
  for (int i = 0; i < fruitstand_size; ++i) {
    fs[i] = create_random_fruit();
  }

}

void go_shopping(vector<Fruit*>& shopping_cart,
                 Fruit* fruitstand[],
                 float& available_money) {
  //I keep shopping while there is more fruit to look at, and until
  //I reach the limit of the number of fruit I'm interested in buying.
  for (int i = 0;
       i < fruitstand_size &&
       shopping_cart.size() < num_fruit_to_buy;
       ++i) {
    if (fruitstand[i]->cost < available_money) {
      //I'm not extremely particular, so I'll take something in the better half
      bool good_enough = fruitstand[i]->sniff(0.5);
      if (good_enough) {
        cout << "I buy this fruit" << endl;
        shopping_cart.push_back(fruitstand[i]);
        available_money -= fruitstand[i]->cost;
        cout << "I now have " << available_money << " left." << endl;
      }
      else {
        cout << "I put it back" << endl;
      }
    }
    else {
      cout << "I put the next fruit back (too expensive)" << endl;
    }
  }
  cout << "Done shopping." << endl << endl;
}

void show_results(vector<Fruit*> shopping_cart, float available_money) {
  cout << "I bought:\n  ";
  for (int i = 0; i < shopping_cart.size(); ++i) {
    Fruit* f = shopping_cart[i];

    //The pointer will be set to NULL if the cast fails
    Banana* b1 = dynamic_cast<Banana*>(f);
    Pineapple* p1 = dynamic_cast<Pineapple*>(f);

    if (b1 != NULL) {
      cout << "a banana";
    }
    else if (p1 != NULL) {
      cout << "a pineapple";
    }

    if (i < shopping_cart.size()-2) {
      cout << ", ";
    }
    else if (i == shopping_cart.size()-2) {
      cout << ", and ";
    }
  }
  cout << endl;
  cout << "I ended my trip with " << available_money << " left over." << endl;
}

int main() {

  float available_money;

  //Create an array of fruit
  Fruit* fruitstand[fruitstand_size];

  //Create a shopping cart
  vector<Fruit*> shopping_cart;

  setup_shopping_trip(available_money, fruitstand);

  go_shopping(shopping_cart, fruitstand, available_money);

  show_results(shopping_cart, available_money);

  return 0;
}
