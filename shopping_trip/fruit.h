#ifndef _FRUIT_H_
#define _FRUIT_H_

/***************************************************************************
* Filename: fruit.h
* Author: Luke Kanuchok
* Date: 2016/11/02
*
* Class: Fruit
*
* Class components:
*   Public data members:
*    - NONE
*   Public methods:
*    - Name: Fruit (parameterized constructor)
*      - Return type: NONE
*      - Arguments:
*        - Name: c
*          - Type: float 
*          - Intent: Cost of the Fruit
*        - Name: q
*          - Type: double 
*          - Intent: Quality of the Fruit
*      - Intent: Construct a fruit. Use a randomly generated number to set
*                desired smell.
*    - Name: sniff
*      - Return type: virtual bool
*      - Arguments:
*        - Name: desired_smell
*        - Type: float
*        - Intent: Level of good smell required.
*      - Intent: Returns true if fruit smells good, false otherwise.
*   Protected data members:
*    - Name: smell_value
*      - Type: float
*      - Intent: How good this fruit smells (0 = Awful, 1 = Awesome.)
*   Protected methods:
*    - NONE
*   Private data members:
*    - NONE
*   Private methods:
*    - NONE

* Example data member
*    - Name:
*      - Type: 
*      - Intent: 

* Example method
*    - Name: 
*      - Return type: 
*      - Arguments:
*        - Name: 
*        - Type: 
*        - Intent: 
*      - Intent: 
***************************************************************************/

#include "salable.h"  //For Salable

class Fruit : public Salable {
  public:
  //- Public data members -

  //- Public methods
  Fruit(float c, double q);
  virtual bool sniff(float desired_smell);

  protected:
  //- Protected data members -
  float smell_value;

  //- Protected methods

  private:
  //- Private data members -

  //- Private methods

};

#endif
