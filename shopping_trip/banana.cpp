/***************************************************************************
* Filename: banana.cpp
* Author: Luke Kanuchok
* Date: 2016/11/02
*
* Class: Banana
*
* Class components:
*   Public data members:
*    - NONE
*   Public methods:
*    - Name: Banana (Parameterized constructor)
*      - Return type: NONE
*      - Arguments:
*        - Name: c
*          - Type: float 
*          - Intent: Cost of the banana
*        - Name: q
*          - Type: double
*          - Intent: Quality of the banana
*      - Intent: Construct a banana
*    - Name: sniff
*      - Return type: bool
*      - Arguments:
*        - Name: desired_smell
*          - Type: float
*          - Intent: How good does the banana need to smell (0 = Don't care,
*                    1 = Must be awesome.)
*      - Intent: Check how the banana smells
*   Protected data members:
*    - NONE
*   Protected methods:
*    - NONE
*   Private data members:
*    - NONE
*   Private methods:
*    - NONE
***************************************************************************/

#include "banana.h"
#include <iostream>

using std::cout;
using std::endl;

Banana::Banana(float c, double q) : Fruit(c, q) {
  cout << "In Banana constructor (cost: " << c;
  cout << ", quality: " << q << ")" << endl;
}

bool Banana::sniff(float desired_smell) {
  bool smells_good = (smell_value >= desired_smell);
  cout << "Banana sniff test: ";
  if (smells_good)
    cout << "PASSES";
  else
    cout << "FAIL";
  
  cout << " (fruits smell: " << smell_value;
  cout << ", desired smell: " << desired_smell << ")" << endl;

  return smells_good;
}

