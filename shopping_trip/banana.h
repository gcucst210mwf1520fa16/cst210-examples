#ifndef _BANANA_H_
#define _BANANA_H_

/***************************************************************************
* Filename: banana.h
* Author: Luke Kanuchok
* Date: 2016/11/02
*
* Class: Banana
*
* Class components:
*   Public data members:
*    - NONE
*   Public methods:
*    - Name: Banana (Parameterized constructor)
*      - Return type: NONE
*      - Arguments:
*        - Name: c
*          - Type: float 
*          - Intent: Cost of the banana
*        - Name: q
*          - Type: double
*          - Intent: Quality of the banana
*      - Intent: Construct a banana
*    - Name: sniff
*      - Return type: bool
*      - Arguments:
*        - Name: desired_smell
*          - Type: float
*          - Intent: How good does the banana need to smell (0 = Don't care,
*                    1 = Must be awesome.)
*      - Intent: Check how the banana smells
*   Protected data members:
*    - NONE
*   Protected methods:
*    - NONE
*   Private data members:
*    - NONE
*   Private methods:
*    - NONE

* Example data member
*    - Name:
*      - Type: 
*      - Intent: 

* Example method
*    - Name: 
*      - Return type: 
*      - Arguments:
*        - Name: 
*          - Type: 
*          - Intent: 
*      - Intent: 
***************************************************************************/

#include "fruit.h"

class Banana : public Fruit {
  public:
  //- Public data members -

  //- Public methods
  Banana(float c, double q);
  bool sniff(float desired_smell);

  protected:
  //- Protected data members -

  //- Protected methods

  private:
  //- Private data members -

  //- Private methods

};

#endif
