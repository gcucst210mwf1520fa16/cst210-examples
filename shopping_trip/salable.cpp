/***************************************************************************
* Filename: salable.cpp
* Author: Luke Kanuchok
* Date: 2016/11/02
*
* Class: Salable
*
* Class components:
*   Public data members:
*    - Name: cost
*      - Type: const float
*      - Intent: Cost of the given salable item
*   Public methods:
*    - Name: check_quality
*      - Return type: bool
*      - Arguments:
*        - Name: desired_quality
*        - Type: double
*        - Intent: Floating point value between 0-1 indicating the desired
*                  quality (0 = I'll buy anything. 1 = Only the best for me.)
*      - Intent: Returns a true if the item matches or exceeds the desired
*                quality, and false otherwise
*   Protected data members:
*    - NONE
*   Protected methods:
*    - Name: Salable (Parameterized constructor)
*      - Return type: NONE
*      - Arguments:
*        - Name: cst
*          - Type: float
*          - Intent: The cost of the new salable item
*        - Name: qual
*          - Type: double
*          - Intent: The quality value for the new salable item
*      - Intent: Construct a salable item
*   Private data members:
*    - Name: base_quality
*      - Type: double
*      - Intent: The base quality value of the salable item. This will be modified
*                in the inherited subclasses.
*   Private methods:
*    - NONE
***************************************************************************/

#include "salable.h"

#include <iostream>

using std::cout;
using std::endl;

bool Salable::check_quality(double desired_quality) {
  return base_quality >= desired_quality;
}

Salable::Salable(float cst, double qual) : cost(cst), base_quality(qual) {
  cout << "In Salable constructor" << endl;
}

