/***************************************************************************
* Filename: pineapple.cpp
* Author: Luke Kanuchok
* Date: 2016/11/02
*
* Class: Pineapple
*
* Class components:
*   Public data members:
*    - NONE
*   Public methods:
*    - Name: Pineapple (Parameterized constructor)
*      - Return type: NONE
*      - Arguments:
*        - Name: c
*          - Type: float 
*          - Intent: Cost of the pineapple
*        - Name: q
*          - Type: double
*          - Intent: Quality of the pineapple
*      - Intent: Construct a pineapple
*    - Name: sniff
*      - Return type: bool
*      - Arguments:
*        - Name: desired_smell
*          - Type: float
*          - Intent: How good does the pineapple need to smell (0 = Don't care,
*                    1 = Must be awesome.)
*      - Intent: Check how the pineapple smells
*   Protected data members:
*    - NONE
*   Protected methods:
*    - NONE
*   Private data members:
*    - NONE
*   Private methods:
*    - NONE
***************************************************************************/

#include "pineapple.h"
#include <iostream>

using std::cout;
using std::endl;

Pineapple::Pineapple(float c, double q) : Fruit(c, q) {
  cout << "In Pineapple constructor (cost: " << c;
  cout << ", quality: " << q << ")" << endl;
}

bool Pineapple::sniff(float desired_smell) {
  bool smells_good = (smell_value >= desired_smell);
  cout << "Pineapple sniff test: ";
  if (smells_good)
    cout << "PASSES";
  else
    cout << "FAIL";
  
  cout << " (fruits smell: " << smell_value;
  cout << ", desired smell: " << desired_smell << ")" << endl;

  return smells_good;
}
