#ifndef _PINEAPPLE_H_
#define _PINEAPPLE_H_

/***************************************************************************
* Filename: pineapple.h
* Author: Luke Kanuchok
* Date: 2016/11/02
*
* Class: Pineapple
*
* Class components:
*   Public data members:
*    - NONE
*   Public methods:
*    - Name: Pineapple (Parameterized constructor)
*      - Return type: NONE
*      - Arguments:
*        - Name: c
*          - Type: float 
*          - Intent: Cost of the pineapple
*        - Name: q
*          - Type: double
*          - Intent: Quality of the pineapple
*      - Intent: Construct a pineapple
*    - Name: sniff
*      - Return type: bool
*      - Arguments:
*        - Name: desired_smell
*          - Type: float
*          - Intent: How good does the pineapple need to smell (0 = Don't care,
*                    1 = Must be awesome.)
*      - Intent: Check how the pineapple smells
*   Protected data members:
*    - NONE
*   Protected methods:
*    - NONE
*   Private data members:
*    - NONE
*   Private methods:
*    - NONE

* Example data member
*    - Name:
*      - Type: 
*      - Intent: 

* Example method
*    - Name: 
*      - Return type: 
*      - Arguments:
*        - Name: 
*          - Type: 
*          - Intent: 
*      - Intent: 
***************************************************************************/

#include "fruit.h"

class Pineapple : public Fruit {
  public:
  //- Public data members -

  //- Public methods
  Pineapple(float c, double q);
  bool sniff(float desired_smell);

  protected:
  //- Protected data members -

  //- Protected methods

  private:
  //- Private data members -

  //- Private methods

};

#endif
