//http://stackoverflow.com/questions/307352/g-undefined-reference-to-typeinfo
#include <iostream>

#include "banana.h"

int main() {

  Banana b(0.95, 0.7);

  b.sniff(0.5);

  return 0;
}
