/***************************************************************************
* Filename: demo_file_io_2.cpp
* Author: Luke Kanuchok
* Date: 10/28/2016
* 
* Demonstrate how File I/O works in C++
* - We're going to open a file for reading in the current directory
* - We're going to open a seperate file for writing (also current directory)
* - For each line that we read in from the first file
*   - We're going to prefix a line number, and write it back to the output
* - Close both files
*
* Newly introduced functions/objects:
* - std::ifstream
*   - http://www.cplusplus.com/doc/tutorial/files/
*   - http://www.learncpp.com/cpp-tutorial/136-basic-file-io/
*   - http://www.cplusplus.com/reference/fstream/ifstream/
* - std::getline
*   - http://www.learncpp.com/cpp-tutorial/136-basic-file-io/
***************************************************************************/

#include <iostream>  //for std::cout, std::endl, std::getline
#include <fstream>   //for std::ofstream
#include <string>    //for std::string

using std::cout;
using std::endl;
using std::string;
using std::ifstream;
using std::ofstream;
using std::getline;

int main() {

  ifstream ifs;   //ifstream objects are an Input File STREAM
  ofstream ofs;   //ofstream objects are an Output File STREAM

  //- Opening input file -
  cout << "Opening the input file..." << endl;
  ifs.open("demo_output_filename.txt"); //Created in Demo 1

  //- Opening output file -
  cout << "Opening the output file..." << endl;
  ofs.open("demo_output_filename2.txt");

  //- Loop over lines -
  cout << "Beginning file loop" << endl;

  int line_counter = 1;
  string line;

  while(getline(ifs, line)) {
    cout << "Got line number " << line_counter << ": \"";
    cout << line << "\"" << endl;

    //- Writing the output line -
    ofs << line_counter << ": " << line << endl;
    line_counter += 1;
  }

  //- Closing files -
  cout << "Closing the input file..." << endl;
  ifs.close();
  cout << "Closing the output file..." << endl;
  ofs.close();

  //- Finished -
  cout << "Done." << endl;

  return 0;
}
