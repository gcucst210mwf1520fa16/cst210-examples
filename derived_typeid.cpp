#include <iostream> //For std::cout, std::endl
#include <typeinfo> //For typeid

using std::cout;
using std::endl;

//- Class definitions -
class A {
  bool func() {}
};

class B : public A {
  bool func() {}
};

class C1 : public B {
  bool func() {}
};

class C2 : public B {
  bool func() {}
};

class X {
  virtual bool func() {}
};
class Y1 : public X {
  bool func() {}
};
class Y2 : public X {
  bool func() {}
};

//- Helper functions -
void check_instance(A* ptr) {

  //cout << "Typeio(ptr)" << typeid(ptr) << endl;
  if (typeid(*ptr) == typeid(A))
    cout << "ptr is an instance of class A" << endl;
  else if (typeid(*ptr) == typeid(B))
    cout << "ptr is an instance of class B" << endl;
  else if (typeid(*ptr) == typeid(C1))
    cout << "ptr is an instance of class C1" << endl;
  else if (typeid(*ptr) == typeid(C2))
    cout << "ptr is an instance of class C2" << endl;
}

void check_instance(X* ptr) {

  //cout << "Typeio(ptr)" << typeid(ptr) << endl;
  if (typeid(*ptr) == typeid(X))
    cout << "ptr is an instance of class X" << endl;
  else if (typeid(*ptr) == typeid(Y1))
    cout << "ptr is an instance of class Y1" << endl;
  else if (typeid(*ptr) == typeid(Y2))
    cout << "ptr is an instance of class Y2" << endl;
}

int main() {

  //- Data -
  A* instances[4];
  X* instances2[3];

  A* ip1 = new A;
  A* ip2 = new B;
  A* ip3 = new C1;
  A* ip4 = new C2;

  X* ip5 = new X;
  X* ip6 = new Y1;
  X* ip7 = new Y2;

  instances[0] = new A;
  instances[1] = new B;
  instances[2] = new C1;
  instances[3] = new C2;

  instances2[0] = new X;
  instances2[1] = new Y1;
  instances2[2] = new Y2;

  //- Check regular inherited instances -

  //Check ip1
  cout << "Checking pointer ip1" << endl;
  if (typeid(*ip1) == typeid(A))
    cout << "ip1 is an instance of class A" << endl;
  else if (typeid(*ip1) == typeid(B))
    cout << "ip1 is an instance of class B" << endl;
  else if (typeid(*ip1) == typeid(C1))
    cout << "ip1 is an instance of class C1" << endl;
  else if (typeid(*ip1) == typeid(C2))
    cout << "ip1 is an instance of class C2" << endl;

  //Check ip2
  cout << "\nChecking pointer ip2" << endl;
  if (typeid(*ip2) == typeid(A))
    cout << "ip2 is an instance of class A" << endl;
  else if (typeid(*ip2) == typeid(B))
    cout << "ip2 is an instance of class B" << endl;
  else if (typeid(*ip2) == typeid(C1))
    cout << "ip2 is an instance of class C1" << endl;
  else if (typeid(*ip2) == typeid(C2))
    cout << "ip2 is an instance of class C2" << endl;

  //Check ip3
  cout << "\nChecking pointer ip3" << endl;
  if (typeid(*ip3) == typeid(A))
    cout << "ip3 is an instance of class A" << endl;
  else if (typeid(*ip3) == typeid(B))
    cout << "ip3 is an instance of class B" << endl;
  else if (typeid(*ip3) == typeid(C1))
    cout << "ip3 is an instance of class C1" << endl;
  else if (typeid(*ip3) == typeid(C2))
    cout << "ip3 is an instance of class C2" << endl;

  //Check ip4
  cout << "\nChecking pointer ip4" << endl;
  if (typeid(*ip4) == typeid(A))
    cout << "ip4 is an instance of class A" << endl;
  else if (typeid(*ip4) == typeid(B))
    cout << "ip4 is an instance of class B" << endl;
  else if (typeid(*ip4) == typeid(C1))
    cout << "ip4 is an instance of class C1" << endl;
  else if (typeid(*ip4) == typeid(C2))
    cout << "ip4 is an instance of class C2" << endl;

  cout << "\nChecking instances:" << endl;
  for (int i = 0; i < 4; ++i) {
    check_instance(instances[i]);
  }

  //- Check polymophic instances -

  //Check ip5
  cout << "\nChecking pointer ip5" << endl;
  if (typeid(*ip5) == typeid(X))
    cout << "ip5 is an instance of class X" << endl;
  else if (typeid(*ip5) == typeid(X))
    cout << "ip5 is an instance of class Y1" << endl;
  else if (typeid(*ip5) == typeid(Y2))
    cout << "ip5 is an instance of class Y2" << endl;

  //Check ip6
  cout << "\nChecking pointer ip6" << endl;
  if (typeid(*ip6) == typeid(X))
    cout << "ip6 is an instance of class X" << endl;
  else if (typeid(*ip6) == typeid(Y1))
    cout << "ip6 is an instance of class Y1" << endl;
  else if (typeid(*ip6) == typeid(Y2))
    cout << "ip6 is an instance of class Y2" << endl;

  //Check ip7
  cout << "\nChecking pointer ip7" << endl;
  if (typeid(*ip7) == typeid(X))
    cout << "ip7 is an instance of class X" << endl;
  else if (typeid(*ip7) == typeid(X))
    cout << "ip7 is an instance of class Y1" << endl;
  else if (typeid(*ip7) == typeid(Y2))
    cout << "ip7 is an instance of class Y2" << endl;

  cout << "\nChecking instances2:" << endl;
  for (int i = 0; i < 3; ++i) {
    check_instance(instances2[i]);
  }

  return 0;
}
