#include <iostream>

#include <fstream>

class Persistent {
  virtual void serialize(std::fstream& os) = 0;

  virtual void deserialize(std::fstream& is) = 0;
};

class Thing : public Persistent {
  public:
  Thing();
  Thing(int num);

  void serialize(std::fstream& os);

  void deserialize(std::fstream& is);

  void print() { std::cout << "num:" << number << std::endl; }

  private:
  int number;
};

Thing::Thing() {
  number = 42;
}

Thing::Thing(int num) {
  number = num;

}

void Thing::serialize(std::fstream& os) {
  std::cout << "Serializing thing:..." << std::endl;
  os << number;
};

void Thing::deserialize(std::fstream& is) {
  std::cout << "Deserializing thing:..." << std::endl;
  is >> number;
};

int main() {

  Thing x(100);
  x.print();

  std::fstream fs("save.file", std::ios::out);
  x.serialize(fs);
  fs.close();


  std::fstream fs2("save.file", std::ios::in);
  Thing x2;
  x2.print();
  x2.deserialize(fs2);
  x2.print();
  
  return 0;
}
