/*******************************************************************************
* Filename: medival_shopping.cpp
* Author: Luke Kanuchok
* Date: 11/16/2016
*
* Description:
* The code below simulates a shopping trip where the user can purchase items
* from a store. The Store class has an inventory and a shopping cart, both of
* class Inventory, used to store objects of class Item. An object of type Item
* has a price (float) and a name (string).
*
* There are several problems with the below code. For example, if you buy
* several items and go to check out, the items that are put into the player's
* inventory are not transferred correctly.
*
* Can you find and fix the problems in this program?
*******************************************************************************/

#include <iostream>
#include <string>
#include <vector>

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::vector;

class Item {
  public:
  Item(float pr, string na) : price(pr), name(na) {}
  float price;
  string name;
};

class Inventory {
  public:
  Inventory() {}
  vector<Item*> items;

  int size() {return items.size();}
  Item* get(int pos) {return items[pos];}

  void add(Item* it) { items.push_back(it); }
  Item* remove(int pos) {
    Item* it = items[pos];
    items.erase(items.begin()+pos);
    return it;
  }

  void print_item_list() {
    for (int i = 0; i < items.size(); ++i) {
      cout << i << ": " << items[i]->name << " (costs ";
      cout << items[i]->price << ")" << endl;
    }
    if (items.size() == 0)
      cout << "Nothing!" << endl;
  }

  float calculate_total_cost() {
    float cost = 0;
    for (int i = 0; i < items.size(); ++i)
      cost += items[i]->price;
    return cost;
  }

  void move_all_items_into(Inventory& other) {
    for (int i = 0; i < items.size(); ++i)
      other.add(remove(i));
  }
};

class Store {
  private:
  Inventory shelves;
  Inventory cart;

  void setup_store() {
    shelves.add(new Item( 0.50, "Apple"));
    shelves.add(new Item( 0.75, "Sandwich"));
    shelves.add(new Item( 1.75, "Roast"));
    shelves.add(new Item( 4.00, "Health Potion"));
    shelves.add(new Item(10.00, "Sword"));
    shelves.add(new Item(20.00, "Chainmail"));
  }

  public:
  Store() {
    setup_store();
  }

  void go_shopping(float& money, Inventory& playerInv) {
    bool not_done_shopping = true;
    char user_input;

    while (not_done_shopping) {
      print_shopping_menu();
      cin >> user_input;

      if (user_input == '1') {
        put_item_into_cart();
      }
      else if (user_input == '2') {
        remove_item_from_cart();
      }
      else if (user_input == '3') {
        cart.print_item_list();
      }
      else if (user_input == '4') {
        checkout(money, playerInv);
      }
      else if (user_input == '5') {
        if (cart.size() > 0) {
          char user_choice;
          cout << "You still have items in your cart. Do you really want to ";
          cout << "quit shopping? \n(Y or y to quit, anything else to ";
	  cout << "keep shopping):";
          cin >> user_choice;
          if (user_choice == 'y' || user_choice == 'Y')
	    not_done_shopping = false; //
        }
	else {
          not_done_shopping = false; //Quit
        }
      }
      else {
        cout << "I'm sorry, I don't understand. Please try again." << endl;
      }
    }
    cout << "Thank you for visiting our establishment." << endl;
  }

  private:
  void print_shopping_menu() {
    cout << "---------------------------------------" << endl;
    cout << "Please pick from the following options:" << endl;
    cout << "1. Put an item in the cart" << endl;
    cout << "2. Put an item back" << endl;
    cout << "3. Print the current shopping cart" << endl;
    cout << "4. Finalize your purchase and check out" << endl;
    cout << "5. Quit shopping" << endl;
    cout << "Enter a number:";
  }

  void put_item_into_cart() {
    int user_choice;
    cout << "Which item do you want to enter in your cart:" << endl;
    shelves.print_item_list();
    cout << "Enter a number:";
    cin >> user_choice;

    if (bound_check_is_ok(user_choice, 0, shelves.size()-1)) {
      cart.add(shelves.get(user_choice));
      cout << "Adding item " << user_choice << " to the cart." << endl;
    }
    else {
      cout << "Error: unknown item." << endl;
    }
  }

  void remove_item_from_cart() {
    int user_choice;
    cout << "Which item do you want to remove from your cart:" << endl;
    cart.print_item_list();
    cout << "Enter a number:";
    cin >> user_choice;
    cart.remove(user_choice);
  }

  void checkout(float& money, Inventory& playerInv) {
    char user_choice;
    cout << "You have the following items in your cart:" << endl;
    cart.print_item_list();

    float cost = cart.calculate_total_cost();
    cout << "This costs a total of " << cost << endl;
    cout << "Your funds are currently " << money << endl;

    if (money >= cost) {
      cout << "You have enough to buy these items." << endl;
      cout << "Do you want to close this transaction? ([Y]/n):";
      cin >> user_choice;
      if (user_choice == 'y' || user_choice == 'Y') {
        //
        cart.move_all_items_into(playerInv);
        money -= cost;
        cout << "Thank you for your purchase." << endl;
      }
      else {
        cout << "Checkout cancelled. Returning to shopping menu." << endl;
      }
    }
    else {
      cout << "I'm sorry, you don't have enough money.";
      cout << " Cancelling checkout." << endl;
      cout << "(You need to put some items back.)" << endl;
    }
  }

  bool bound_check_is_ok(int user_choice, int l, int u) {
    if (user_choice < l) {
      cout << "I'm sorry, your choice of " << user_choice << " is below the allowed ";
      cout << "lower bound of " << l << endl;
      return false;
    }
    else if (user_choice > u) {
      cout << "I'm sorry, your choice of " << user_choice << " is above the allowed ";
      cout << "upper bound of " << u << endl;
      return false;
    }
    return true;
  }
};


int main() {
  float money = 20.00;
  Store store;
  Inventory playerInv;

  store.go_shopping(money, playerInv);

  cout << "I bought:" << endl;
  playerInv.print_item_list();
  cout << "and have " << money << " money left over." << endl;

  return 0;
}
