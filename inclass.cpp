#include <cstdio>
#include <iostream>

using namespace std;

//Function will go here
float myFunc(int x, int y, int z) {
  float total = x + y + z;
  cout << "total:" << total << endl;
  cout << "total/3 = " << total / 3 << endl;
  return total / 3;
}

int main() {
  int val1 = 2;
  int val2 = 7;
  int val3 = 29;
  float result;

  //Call function here
  result = myFunc(val1, val2, val3);

  cout << "Result: " << result << endl;
}
