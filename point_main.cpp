#include <iostream>
#include "point.h"

using std::cout;
using std::endl;

int main() {
  Point2D point1(5,10);
  Point2D point2(20,40);

  Point2D point3 = point1.sum(point2);
  Point2D point4 = point2 + point3;

  Point2D point5 = point3.scale(5);

  Point2D point6 = point3 * 10;
  //Point2D point7 = 10 * point3;

  Point2D point8 = point1.difference(point2);
  //Point2D point9 = point2 - point3;

  cout << "point1.x: " << point1.x << endl;
  cout << "point1.y: " << point1.y << endl;
  cout << "point2.x: " << point2.x << endl;
  cout << "point2.y: " << point2.y << endl;
  cout << "point3.x: " << point3.x << endl;
  cout << "point3.y: " << point3.y << endl;
  cout << "point4.x: " << point4.x << endl;
  cout << "point4.y: " << point4.y << endl;
  cout << "point5.x: " << point5.x << endl;
  cout << "point5.y: " << point5.y << endl;
  cout << "point6.x: " << point6.x << endl;
  cout << "point6.y: " << point6.y << endl;
  //cout << "point7.x: " << point7.x << endl;
  //cout << "point7.y: " << point7.y << endl;
  cout << "point8.x: " << point8.x << endl;
  cout << "point8.y: " << point8.y << endl;
  //cout << "point9.x: " << point9.x << endl;
  //cout << "point9.y: " << point9.y << endl;
  return 0;
}
