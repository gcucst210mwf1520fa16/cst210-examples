/***************************************************************************
* Filename: demo_file_io_3.cpp
* Author: Luke Kanuchok
* Date: 10/28/2016
* 
* Demonstrate how File I/O works in C++
* - We're going to open a file for reading in the current directory
* - We're going to open a seperate file for writing (also current directory)
* - For each line that we read in from the first file
*   - We're going to prefix a line number
*   - If it's a number on the line, we're going to indent the line 4 spaces
* - Close both files
*
* Newly introduced functions/objects:
* - std::atoi
***************************************************************************/

#include <iostream>  //for std::cout, std::endl, std::getline
#include <fstream>   //for std::ofstream
#include <string>    //for std::string
#include <cstdlib>   //for std::atoi

using std::cout;
using std::endl;
using std::string;
using std::ifstream;
using std::ofstream;
using std::atoi;

int main() {

  ifstream ifs;   //ifstream objects are an Input File STREAM
  ofstream ofs;   //ofstream objects are an Output File STREAM

  //- Opening input file -
  cout << "Opening the input file..." << endl;
  ifs.open("demo_output_filename.txt"); //Created in Demo 1

  //- Opening output file -
  cout << "Opening the output file..." << endl;
  ofs.open("demo_output_filename3.txt");

  //- Loop over lines -
  cout << "Beginning file loop" << endl;

  int line_counter = 1;
  string line;

  while(std::getline(ifs, line)) {
    cout << "Got line number " << line_counter << ": \"";
    cout << line << "\"" << endl;

    //- Determine if the line is a number -
    bool is_num = false;
    int num = atoi(line.c_str());
    if (num != 0) {
      //Success
      cout << "  Got a number: " << num << endl;
      is_num = true;
    }

    //- Test based on atoi() success -
    if (is_num) {
      ofs << "    ";  //Enter our four spaces
    }

    //- Writing the rest of the line -
    ofs << line_counter << ": " << line << endl;
    line_counter += 1;
  }
  

  //- Closing files -
  cout << "Closing the input file..." << endl;
  ifs.close();
  cout << "Closing the output file..." << endl;
  ofs.close();

  //- Finished -
  cout << "Done." << endl;

  return 0;
}
