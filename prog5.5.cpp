/*

Create two classes DM and DB which store the value of distances. DM stores distances in
metres and centimetres and DB in feet and inches. Write a program that can read values for
the class objects and add one object of DM with another object of DB. W E B
Use a friend function to carry out the addition operation. The object that stores the results may
be a DM object or DB object, depending on the units in which the results are required.
The display should be in the format of feet and inches or metres and centimetres depending on
the object on display.

*/

#include <iostream>

using namespace std;

class DB;

class DM {
  //measurements will be stored in meters
  double meters;
  
  public:
  DM() {
    meters = 0.0;
  }

  DM(double m) {
    meters = m;
  }

  DM(double m, double cm) {
    set_dist(m, cm);
  }

  void set_dist(double m, double cm) {
    meters = m + (cm / 100);
  }

  double get_dist(void) {
    return meters;
  }

  double add_dist(DM other) {
    return meters + other.meters;
  }

  double add_dist(DB other);
};

const int inch_in_foot = 12;

class DB {
  //measurements will be stored in inches
  double inches;
  
  public:
  DB() {
    inches = 0.0;
  }

  void set_dist(double ft, double in) {
    inches = in + ft*inch_in_foot;
  }

  double get_dist(void) {
    return inches;
  }

  double add_dist(DB other) {
    return inches + other.inches;
  }

  void print() {
    int feet = inches / 12;
    cout << feet << "' " << inches - (feet*12) << "\"" << endl;
  }

  friend double DM::add_dist(DB other);
};

double DM::add_dist(DB other) {
  return meters + (other.inches*2.54 / 100);
}

int main ()
{

  DM dist_m1{40, 105};
  DM dist_m2{45, 113};
  DB dist_i1;
  DB dist_i2;

  //dist_m.set_dist(25, 40);
  cout << "dist_m1: " << dist_m1.get_dist() << " m.\n";
  cout << "dist_m2: " << dist_m2.get_dist() << " m.\n";
  cout << "sum    : " << dist_m1.add_dist(dist_m2) << " m.\n";

  dist_i1.set_dist(12, 2);
  dist_i2.set_dist(16, 1);
  cout << "dist_i1: " << dist_i1.get_dist() << "\".\n";
  cout << "dist_i2: " << dist_i2.get_dist() << "\".\n";

  cout << "sum    : " << dist_i1.add_dist(dist_i2) << "\".\n";

  cout << "dist_m1 + dist_i1: " << dist_m1.add_dist(dist_i1) << " m.\n";

  cout << "dist_i1: ";
  dist_i1.print();

  return 0;
}

