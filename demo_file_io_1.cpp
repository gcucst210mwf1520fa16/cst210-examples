/***************************************************************************
* Filename: demo_file_io_1.cpp
* Author: Luke Kanuchok
* Date: 10/28/2016
* 
* Demonstrate how File I/O works in C++
* - We're going to open a file for writing in the current directory
* - Write some test to it
* - Close it again
*
* Newly introduced functions/objects:
* - std::ofstream
*   - http://www.cplusplus.com/doc/tutorial/files/
*   - http://www.learncpp.com/cpp-tutorial/136-basic-file-io/
*   - http://www.cplusplus.com/reference/fstream/ofstream/
***************************************************************************/

#include <iostream>  //for std::cout, std::endl
#include <fstream>   //for std::ofstream

using std::cout;
using std::endl;
using std::ofstream;

int main() {

  ofstream of;   //ofstream objects are a Output File STREAM

  //- Opening a file -
  cout << "Opening the file..." << endl;
  of.open("demo_output_filename.txt");

  //- Writing to an open file -
  cout << "Writing to the file..." << endl;
  of << "This is a test of the output file IO in C++." << endl;
  for (int i = 10; i <= 100; i += 10) {
    of << i << endl;
  } 
  of << "The quick brown fox jumps over the lazy dog." << endl;

  //- Closing a file -
  cout << "Closing the file..." << endl;
  of.close();

  //- Finished -
  cout << "Done." << endl;

  return 0;
}
