/*******************************************************************************
* Filename: read_data_into_class.cpp
* Author: Luke Kanuchok
* Date: 2018/04/01
*
* This file will give an example of how to read some data in from a file into
* an object.
*
* Example data file (exclude the """ lines):
* """
10 20 Sword 
15 30 Axe
20 40 Halberd
* """
*
* There are three methods that we can use:
* 1.) Create a blank item, load the data, and use mutators (setters) to set the
*     values.
* 2.) Load the data, and create the item, passing the values into the
*     constructor.
* 3.) Create the item, passing in the file stream, and allowing the item to get
*     its own data out.
* 
*******************************************************************************/


#include <iostream>
#include <string>
#include <fstream>

using std::cout;
using std::endl;
using std::string;
using std::ifstream;

class Weapon {
  public:
  Weapon(); //Method 1: Create a blank item first
  Weapon(int pr, int wei, string na); //Method 2: pass in the needed values
  Weapon(ifstream& ifs); //Method 3: Allow the item to pull out its own values

  //Mutators and accessors
  void setPrice(int val);
  int getPrice();
  void setWeight(int val);
  int getWeight();
  void setName(string val);
  string getName();

  private:
  int price;
  int weight;
  string name;
};

Weapon::Weapon() {} //Method 1: Create a blank item first


Weapon::Weapon(int pr, int wei, string na) :
  price(pr), weight(wei), name(na) {} //Method 2: pass in the needed values


Weapon::Weapon(ifstream& ifs) { //Method 3: Allow the item to pull out its own values
  ifs >> price >> weight >> name;
}


//Mutators and accessors
void Weapon::setPrice(int val) {
  price = val;
}

int Weapon::getPrice() {
  return price;
}

void Weapon::setWeight(int val) {
  weight = val;
}

int Weapon::getWeight() {
  return weight;
}

void Weapon::setName(string val) {
  name = val;
}

string Weapon::getName() {
  return name;
}

int main() {
  ifstream inFile("input_file.txt");
  int price_data, weight_data;
  string name_data;

  //Get the first set of data
  inFile >> price_data >> weight_data >> name_data;

  //Method 1: Create a blank item first
  Weapon w_1;

  //Now, fill the object with values
  w_1.setPrice(price_data);
  w_1.setWeight(weight_data);
  w_1.setName(name_data);


  //Method 2: Pass in the needed values
  //Get the second set of data
  inFile >> price_data >> weight_data >> name_data;

  //Create the second weapon using the loaded data
  Weapon w_2(price_data, weight_data, name_data);


  //Method 3: Allow the item to pull out its own values
  Weapon w_3(inFile);

  //Print the weapons
  cout << "1.) " << w_1.getName() << ", price " << w_1.getPrice()
       << ", weight " << w_1.getWeight() << endl;
  cout << "2.) " << w_2.getName() << ", price " << w_2.getPrice()
       << ", weight " << w_2.getWeight() << endl;
  cout << "3.) " << w_3.getName() << ", price " << w_3.getPrice()
       << ", weight " << w_3.getWeight() << endl;

  return 0;

}
