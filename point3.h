/*
Class: point
Author: Luke Kanuchok
Date: 10/10/2016
*/

class Point2D {
  public:
  
  //Public data
  double x;
  double y;

  //Public methods
  Point2D() {  //Default constructor
    x = 0.0;
    y = 0.0;
  }

  Point2D(double _x, double _y) {  //Parameterized constructor
    x = _x;
    y = _y;
  }

  Point2D(const Point2D& p2d) { //Copy constructor
    x = p2d.x;
    y = p2d.y;
  }

  Point2D sum(Point2D& second) {
    return Point2D(x + second.x, y + second.y);
  }

  Point2D difference(Point2D& second) {
    return Point2D(x - second.x, y - second.y);
  }

  Point2D scale(double multiplier) {
    return Point2D(x*multiplier, y*multiplier);
  }

  Point2D scale(double divisor, double foobar) {
    return Point2D(x*divisor, y*divisor);
  }

  Point2D operator+(Point2D& other) {
    return Point2D(x + other.x, y + other.y);
  }

  Point2D operator-(Point2D& other) {
    return Point2D(x - other.x, y - other.y);
  }

  Point2D operator*(double val) {
    return Point2D(x*val, y*val);
  }

  private:
  //Private data: None
  //Private methods: none
};

Point2D operator*(double val, const Point2D& p2d) {
  return Point2D(p2d.x*val, p2d.y*val);
}
