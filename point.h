/*
Class: point
Author: Luke Kanuchok
Date: 10/10/2016
*/

class Point2D {
  public:
  
  //Public data
  int x;
  int y;

  //Public methods
  Point2D() {  //Default constructor
    x = 0;
    y = 0;
  }

  Point2D(int _x, int _y) {  //Parameterized constructor
    x = _x;
    y = _y;
  }

  Point2D sum(Point2D& second) {
    return Point2D(x + second.x, y + second.y);
  }

  Point2D difference(Point2D& second) {
    return Point2D(x - second.x, y - second.y);
  }

  Point2D scale(int multiplier) {
    return Point2D(x*multiplier, y*multiplier);
  }

  Point2D scale(int divisor, int foobar) {
    return Point2D(x*divisor, y*divisor);
  }

  Point2D operator+(Point2D& other) {
    return Point2D(x + other.x, y + other.y);
  }

  Point2D operator*(int val) {
    //TODO: Write me!
    return Point2D(x*val, y*val);
  }

  //Point2D operator*(int val) {
    ////TODO: Write me!
    //return Point2D(x*val.x, y*val.y);
  //}

  private:
  //Private data: None
  //Private methods: none
};
