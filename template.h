#ifndef __H_
#define __H_

/***************************************************************************
* Filename: 
* Author: 
* Date: YYYY/MM/DD
*
* Class: 
*
* Class components:
*   Public data members:
*    - NONE
*   Public methods:
*    - NONE
*   Protected data members:
*    - NONE
*   Protected methods:
*    - NONE
*   Private data members:
*    - NONE
*   Private methods:
*    - NONE

* Example data member
*    - Name:
*      - Type: 
*      - Intent: 

* Example method
*    - Name: 
*      - Return type: 
*      - Arguments:
*        - Name: 
*        - Type: 
*        - Intent: 
*      - Intent: 
***************************************************************************/

class  {
  public:
  //- Public data members -

  //- Public methods

  protected:
  //- Protected data members -

  //- Protected methods

  private:
  //- Private data members -

  //- Private methods

};

#endif
