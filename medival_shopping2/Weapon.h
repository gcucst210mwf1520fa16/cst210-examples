#ifndef _WEAPON_H_
#define _WEAPON_H_

/***************************************************************************
* Filename: 
* Author: 
* Date: YYYY/MM/DD
*
* Class: 
*
* Class components:
*   Public data members:
*    - NONE
*   Public methods:
*    - NONE
*   Protected data members:
*    - NONE
*   Protected methods:
*    - NONE
*   Private data members:
*    - NONE
*   Private methods:
*    - NONE

* Example data member
*    - Name:
*      - Type: 
*      - Intent: 

* Example method
*    - Name: 
*      - Return type: 
*      - Arguments:
*        - Name: 
*        - Type: 
*        - Intent: 
*      - Intent: 
***************************************************************************/

#include "Item.h"

class Weapon : public Item {
  public:
  Weapon(float pr, string na, int dmg) :
  Item(pr, na), damage(dmg) {}

  int getDamage();

  private:
  int damage;
};

#endif
