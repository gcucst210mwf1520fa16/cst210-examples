#ifndef _ITEM_H_
#define _ITEM_H_

/***************************************************************************
* Filename: 
* Author: 
* Date: YYYY/MM/DD
*
* Class: 
*
* Class components:
*   Public data members:
*    - NONE
*   Public methods:
*    - NONE
*   Protected data members:
*    - NONE
*   Protected methods:
*    - NONE
*   Private data members:
*    - NONE
*   Private methods:
*    - NONE

* Example data member
*    - Name:
*      - Type: 
*      - Intent: 

* Example method
*    - Name: 
*      - Return type: 
*      - Arguments:
*        - Name: 
*        - Type: 
*        - Intent: 
*      - Intent: 
***************************************************************************/

#include <string>

using std::string;

class Item {
  public:
  Item(float pr, string na) : price(pr), name(na) {}
  float price;
  string name;
};


#endif
