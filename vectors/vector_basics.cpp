/*******************************************************************************
* Filename: vector_basics.cpp
* Author: Luke Kanuchok
* Date: 2018/02/13
*
* This is a same code file to learn how to use vector, a dynamic container that
* is fairly easy to use. We will not be learning about how the vector works
* internally, but will save that for a later example.
*
* We will explore the following areas:
* - Creating a vector
* - Checking if a vector is empty
* - Checking the size of a vector
* - Putting an element into a vector...
*   - ...at the beginning
*   - ...on the end
* - 
*
*******************************************************************************/
#include <iostream>  //For cout and endl
#include <vector>    //For vector

using namespace std;

void print_vector_of_ints(vector<int>& v) {
  for (int i = 0; i < v.size(); ++i) {
    cout << "  item " << i << ": " << v[i] << endl;
  }
}

void print_vector_of_strings(vector<string>& v) {
  for (int i = 0; i < v.size(); ++i) {
    cout << "  item " << i << ": " << v[i] << endl;
  }
}

void test_vector_of_ints() {
  cout << "## Working with a vector of integers...\n";

  // - Creating a vector -
  //We create a vector by specifying the type of item to be held in the vector.
  cout << "Creating the vector\n\n";
  vector<int> vec;

  //This vector is an object, with methods and data inside. The data is
  //generally internal/private. The methods are where we have interest.


  // - Checking the status of the vector -
  cout << endl;

  //This vector has no items in it. This can be shown by using the empty()
  //method:
  cout << "Checking whether the vector is empty...\n";
  bool vector_is_empty = vec.empty();

  if (vector_is_empty) {
    cout << "  Vector vec is empty" << endl;
  }
  else {
    cout << "  Vector vec is not empty" << endl;
  }

  //We can also check the number of items in the vector by checking it's size
  //by using the size() method:
  cout << "\nChecking the size of the vector:\n";
  int vec_size = vec.size();
  cout << "There are " << vec_size << " items in the vector." << endl;


  // - Adding values to the vector -
  cout << endl;

  //Now, we will put some items into the vector using the push_back() method:
  cout << "Pushing value '5' into vec\n";
  vec.push_back(5);

  //We can also use a variable as a argument to push_back():
  cout << "Pushing value '42' into vec\n";
  int x = 42;
  vec.push_back(x);

  //Now that we have one or more values in vec, the size is now changed:
  cout << "\nChecking the size of the vector:\n";
  vec_size = vec.size();  //Reusing an existing variable
  cout << "There are " << vec_size << " items in the vector." << endl;


  // - Reading values from the vector -
  cout << endl;

  //We don't have a specific method for printing the vector, as the items in
  //the vector may not be simple items (like int, float, bool, etc.)
  //We will just iterate over the items in the vector and print them out as
  //single items. To access them, we can use either the at() method, or the
  //standard index operator []:

  cout << "Checking the item in position 0 using at():\n";
  cout << vec.at(0) << endl;

  cout << "Checking the item in position 1 using vec[]:\n";
  cout << vec[1] << endl;

  //We could iterate over the entire whole vector using a loop (as we know
  //how many values are in the vector using size()), and printing the values:
  cout << "Printing all of the items in the vector:\n";
  for (int i = 0; i < vec.size(); ++i) {
    cout << "  item " << i << ": " << vec[i] << endl;
  }


  // - Adding more values to the vector -
  cout << endl;

  //We can push items into another place in the vector using insert()
  //To do this, we need to use a special index called an iterator.
  //An iterator can be obtained in multiple ways, which we will discuss later.
  //The standard beginning iterator is retrieved by calling vec.begin(), which
  //returns an iterator to the first item in the list. We can "add" integer
  //values to this iterator to increment it along the vector:
  cout << "Inserting value 100 into vec, in position 0:\n";
  vector<int>::iterator it = vec.begin();
  vec.insert(it, 100);
  //NOTE: 'it' becomes invalid after the vector changes, which is does when
  //we insert a new value. We can always create another as we just did.

  //Print vec to see result (using function to print)
  print_vector_of_ints(vec);

  //Inserting another value into position 1, in a similar manner, but using
  //the existing iterator stepped forward one position:
  cout << "Inserting another item, 200, into position 1:\n";
  vec.insert(vec.begin()+1, 200);

  //Print vec to see result (using function to print)
  print_vector_of_ints(vec);

  // - Removing values from the vector -
  cout << endl;

  //We should be able to remove items as well, using the erase() method.
  //This method also takes an iterator (for removing a single item), or two
  //iterators to remove a range of items:
  cout << "Erasing third item ('5' is in position 2) from vec:\n";
  //NOTE: We should save off the item(s) if we want to use them after the
  //erase() call:
  int val = vec.at(2);
  vec.erase(vec.begin()+2);
  cout << "Old value in previous position 2:\n";
  cout << val << endl;

  cout << "The rest of the vector, after the erase():\n";
  //Print vec to see result (using function to print)
  print_vector_of_ints(vec);
}
  
void test_vector_of_strings() {
  cout << "## Working with a vector of strings...\n";

  // - Creating a vector -
  //We create a vector by specifying the type of item to be held in the vector.
  cout << "Creating the vector\n\n";
  vector<string> vec;

  //This vector is an object, with methods and data inside. The data is
  //generally internal/private. The methods are where we have interest.


  // - Checking the status of the vector -
  cout << endl;

  //This vector has no items in it. This can be shown by using the empty()
  //method:
  cout << "Checking whether the vector is empty...\n";
  bool vector_is_empty = vec.empty();

  if (vector_is_empty) {
    cout << "  Vector vec is empty" << endl;
  }
  else {
    cout << "  Vector vec is not empty" << endl;
  }

  //We can also check the number of items in the vector by checking it's size
  //by using the size() method:
  cout << "\nChecking the size of the vector:\n";
  int vec_size = vec.size();
  cout << "There are " << vec_size << " items in the vector." << endl;


  // - Adding values to the vector -
  cout << endl;

  //Now, we will put some items into the vector using the push_back() method:
  cout << "Pushing value \"Hello\" into vec\n";
  vec.push_back("Hello");

  //We can also use a variable as a argument to push_back():
  cout << "Pushing value \"C++\" into vec\n";
  string x = "C++";
  vec.push_back(x);

  //Now that we have one or more values in vec, the size is now changed:
  cout << "\nChecking the size of the vector:\n";
  vec_size = vec.size();  //Reusing an existing variable
  cout << "There are " << vec_size << " items in the vector." << endl;


  // - Reading values from the vector -
  cout << endl;

  //We don't have a specific method for printing the vector, as the items in
  //the vector may not be simple items (like int, float, bool, etc.)
  //We will just iterate over the items in the vector and print them out as
  //single items. To access them, we can use either the at() method, or the
  //standard index operator []:

  cout << "Checking the item in position 0 using at():\n";
  cout << vec.at(0) << endl;

  cout << "Checking the item in position 1 using vec[]:\n";
  cout << vec[1] << endl;

  //We could iterate over the entire whole vector using a loop (as we know
  //how many values are in the vector using size()), and printing the values:
  cout << "Printing all of the items in the vector:\n";
  for (int i = 0; i < vec.size(); ++i) {
    cout << "  item " << i << ": " << vec[i] << endl;
  }

  // - Adding more values to the vector -
  cout << endl;

  //We can push items into another place in the vector using insert()
  //To do this, we need to use a special index called an iterator.
  //An iterator can be obtained in multiple ways, which we will discuss later.
  //The standard beginning iterator is retrieved by calling vec.begin(), which
  //returns an iterator to the first item in the list. We can "add" integer
  //values to this iterator to increment it along the vector:
  cout << "Inserting value GCU into vec, in position 0:\n";
  vector<string>::iterator it = vec.begin();
  vec.insert(it, "GCU");
  //NOTE: 'it' becomes invalid after the vector changes, which is does when
  //we insert a new value. We can always create another as we just did.

  //Print vec to see result (using function to print)
  print_vector_of_strings(vec);

  //Inserting another value into position 1, in a similar manner, but using
  //the existing iterator stepped forward one position:
  cout << "Inserting another item, \"Lopes\", into position 1:\n";
  vec.insert(vec.begin()+1, "Lopes");

  //Print vec to see result (using function to print)
  print_vector_of_strings(vec);


  // - Removing values from the vector -
  cout << endl;

  //We should be able to remove items as well, using the erase() method.
  //This method also takes an iterator (for removing a single item), or two
  //iterators to remove a range of items:
  cout << "Erasing third item ('Hello' is in position 2) from vec:\n";
  //NOTE: We should save off the item(s) if we want to use them after the
  //erase() call:
  string val = vec.at(2);
  vec.erase(vec.begin()+2);
  cout << "Old value in previous position 2:\n";
  cout << val << endl;

  cout << "The rest of the vector, after the erase():\n";
  //Print vec to see result (using function to print)
  print_vector_of_strings(vec);
}
  
int main() {
  test_vector_of_ints();
  cout << endl << "####################" << endl;
  test_vector_of_strings();
  cout << "\nDone\n";
}
