#include <iostream>
using namespace std;

float myAvg(int * arr, int num_vals) {
  float result = 0;
  for (int i = 0; i < num_vals; ++i) {
    result = result + arr[i];
  }
  result = result / num_vals;
  return result;
}

int main() {

  int x = 10;
  int * ptr;

  int * p_arr;

  ptr = &x;

  cout << " ptr = " << ptr << endl;
  cout << "*ptr = " << *ptr << endl;

  p_arr = new int[5];
  p_arr[0] = 100;
  p_arr[1] = 101;
  p_arr[2] = 102;
  p_arr[3] = 103;
  p_arr[4] = 107;

  for (int i = 0; i < 5; ++i) {
    cout << p_arr[i] << "  ";
  }
  cout << endl;

  float f_val = myAvg(p_arr, 5);
  cout << "f_val = " << f_val << endl;
  
  delete [] p_arr;

  p_arr = &x;
  cout << " p_arr = " << p_arr << endl;
  cout << "*p_arr = " << *p_arr << endl;

  return 0;
}
