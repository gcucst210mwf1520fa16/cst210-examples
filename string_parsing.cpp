#include <iostream> //For std::cout, std::endl
#include <string>   //For std::string
#include <sstream>  //For std::stringstream
#include <cstdio>   //For std::sscanf

using std::cout;
using std::endl;
using std::string;
using std::stringstream;
using std::sscanf;

struct ItemData {
  char   type;
  int    cost;
  float  weight;
  string name;
};
void print_itemdata(ItemData& item) {
  cout << "item:" << endl;
  cout << "  type: " << item.type << endl;
  cout << "  cost: " << item.cost << endl;
  cout << "  weight: " << item.weight << endl;
  cout << "  name: \"" << item.name << "\"" << endl;
}

template <class T>
T getValueUsingDelim(stringstream& ss, const char delim) {
  string val_str;
  getline(ss, val_str, delim);
  stringstream temp_ss(val_str);
  T val;
  temp_ss >> val;
  return val;
}

int main() {

  //- Method 1: sscanf() -

  const string input_value = "W 5 10.54 Bronze hammer";
  ItemData item;

  //We use a character array, because we need to have a set storage location
  //to save the read characters. Strings don't work in the necessary manner.
  char char_arr[50];  //50 is chosen to comfortably fit an expected name.
  //We read a character (%c) into the item.type memory location.
  //We read a decimal value (%d) into the item.cost memory location.
  //We read a floating point value (%f) into the item.weight memory location.
  //We read a set of characters (%[]) that are not (^) a newline (\n) into char_arr.
  sscanf(input_value.c_str(), "%c %d %f %[^\n]", &item.type, &item.cost,
         &item.weight, char_arr);
  item.name = string(char_arr);

  cout << "Using sscanf():" << endl;
  cout << "Original string: \"" << input_value << "\"" << endl;
  print_itemdata(item);

  //- Method 2: basic stringstream() -
  const string input_value2 = "I 2 .75 Health potion";
  stringstream ss(input_value2);
  ItemData item2;
  ss >> item2.type >> item2.cost >> item2.weight >> item2.name;

  cout << "Using stringstream():" << endl;
  cout << "Original string: \"" << input_value2 << "\"" << endl;
  print_itemdata(item2);

  //Note that using the stringstream only gets up until the next delimiter
  //(a space) into the item2.name. To get around this, we will need to go to
  //further "lengths".

  //- Method 3: advanced stringstream() -
  const string input_value3 = "A 300 500 Iron breastplate";
  stringstream advss(input_value3);
  ItemData item3;
  advss >> item3.type >> item3.cost >> item3.weight; // >> item3.name;

  std::stringstream::pos_type pos = advss.tellg();

  cout << "String pos:" << pos << endl;
  //The "pos+1" comes from the fact that pos is the position of the whitespace
  //just before "Iron". Without this, "input_value3.substr(pos)" would give 
  //" Iron breastplate". (Comment out the "pos += 1;" to see this.)
  pos += 1;
  item3.name = input_value3.substr(pos);

  cout << "Using advanced stringstream():" << endl;
  cout << "Original string: \"" << input_value3 << "\"" << endl;
  print_itemdata(item3);

  //- Method 4: stringstream() and a non-whitespace delimiter -
  //NOTE: getValueUsingDelim is a function written above.
  const string input_value4 = "W|50|12|Wood bow of the forest elves|";
  stringstream ss_nwd(input_value4);
  ItemData item4;
  item4.type   = getValueUsingDelim<char>(ss_nwd, '|');
  item4.cost   = getValueUsingDelim<int>(ss_nwd, '|');
  item4.weight = getValueUsingDelim<int>(ss_nwd, '|');
  getline(ss_nwd, item4.name, '|');
  print_itemdata(item4);
  
  return 0;
}

